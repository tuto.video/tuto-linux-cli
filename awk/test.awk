BEGIN { print "Debut du traitement" }
BEGIN {
   print "Ce script affiche la liste des"
   print "villes qui ont accueilli les JO..."
   print "----------------------------------"
   NB_VILLES=0
}
{
   print $1
   NB_VILLES++
}
END {
   print "----------------------------------"
   print "J'ai fini ! Il y a " NB_VILLES " villes"
}